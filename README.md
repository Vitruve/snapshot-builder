# Snapshot Builder

## Usage example

OBS: the "aerospike" module must be installed in the main project. If not, this modue will produce an error. 

```javascript
var EventStoreRange = require('geteventstore-range');
var esRange = new EventStoreRange(configs.eventStore);

var builder = new snapshotBuilder({
  category: 'apparel',
  esRange: esRange,
  aerospike: yourAerospikeClient,
  aerospikeNamespace: 'someNamespace',
  eventsHandlers: eventsHandlers
});

if(version == 'latest'){
  return builder.getLatest(itemId, function(err, data){
    console.log(err, data);
  });
} else {
  return builder.getSpecificVersion(itemId, version, function(err, data){
    console.log(err, data);
  });
}
```