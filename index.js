var 
  Aerospike = require('aerospike'),
  Key = Aerospike.Key,
  AEROSPIKE_ERR_RECORD_NOT_FOUND = Aerospike.status.AEROSPIKE_ERR_RECORD_NOT_FOUND;

module.exports = function(opts){

    this.opts = opts || {};
    this.eventsIndex = 0;

    this.debug = this.opts.debug || false;
    
    this.log = function(msg){
      if(this.debug == true){
        console.log('[Snapshot Builder] ', msg);
      }
    }

    this.onBeforeSnapshotSave = this.opts.onBeforeSnapshotSave || function(item, handlers, hookCallback){ hookCallback(null, item) };

    this.getLatest = function(itemId, callback){
      var 
        me = this,
        streamName = this.opts.category + '-' + itemId,
        item,
        latestItemKey = new Key(this.opts.aerospikeNamespace, this.opts.category + 's', itemId + ':latest');
      // let's check the Item
      me.log('getting the latest version...');
      this.opts.aerospike.get(latestItemKey, (error, record, meta) => {
        // no item... let's build it!
        if (error && error.code === AEROSPIKE_ERR_RECORD_NOT_FOUND) {
          me.log('item latest snapshot not found');
          // if we don't find the latest version, which is always written, no matter the version,
          // so we don't have any version. We can still check in the eventStore for the stream existence
          me.opts.esRange.getStreamRange(streamName, 'start', 'end', function(error, events){
            // if there is no stream or events, item doesn't exist. Just return an error
            if(!events){
              me.log('And no Stream too. not found');
              return callback({ entity: me.opts.category, type:'not-found', uuid: itemId, notify_user: true });
            }
            if(events.length == 0) {
              me.log('And no Stream too. not found');
              return callback({ entity: me.opts.category, type:'not-found', uuid: itemId, notify_user: true });
            }
            me.log('Yes we have the stream');
            // otherwise if there are events, build the item
            return me.rebuildItemFromEvents(events, null, function(err, item){
              // we didn't find the latest, it means that the item was never built.
              // so we will store it
              var lastEventNumber = events[events.length - 1].eventNumber;

              // and hook to perform some operations before saving
              me.onBeforeSnapshotSave(item, me.opts.eventsHandlers, function(hookErr, finalData){
                var newRecord = {
                  key: itemId + ':latest',
                  // the latest version number
                  version: lastEventNumber,
                  // a flag to check if there are new event to pull
                  // @TODO: set dirty to 0. temporarily always dirty to force events pull
                  dirty: 1, 
                  // the item data itself
                  data: JSON.stringify(finalData)
                };
                // store it, creating the latest for the first time (no need to wait)
                return me.opts.aerospike.put(latestItemKey, newRecord, function(error, responseRecord){
                  me.log('Snapshot built and saved for the first time');
                  // return data!
                  callback(null, finalData);
                });
              });
              
            });
            
          });

        // some error
        } else if (error) {
          me.log('Some error', error);
          return callback(error);

        // we found the item snapshot
        } else {
          me.log('Yes we have the snapshot');
          // if we want the latest, check if it's 'dirty' and if it is dirty then pull the deltas.
          if(record.dirty == 1){ // for now, its always dirty
            me.log('Snapshot is dirty');
            // get events from the snapshot event number to the end of the stream
            return me.opts.esRange.getStreamRange(streamName, record.version + 1, 'end', function(error, events){
              // some error?
              if(error){
                if(error == 'Target Event out of range'){
                  me.log('Error: Target Event out of range. Return anyway.');
                  return callback(null, JSON.parse(record.data));
                } else {
                  me.log('Error:', error);
                  return callback({ entity: me.opts.category, type:'read-error', uuid: itemId, notify_user: true });
                }
              }
              me.log('got ' + events.length + ' events. aggregate with snapshot');
              me.log('the snapshot data:', record);
              // calculate the deltas
              return me.rebuildItemFromEvents(events, JSON.parse(record.data), function(err, item){
                if(err) return callback(err);
                var lastEventNumber = events[events.length - 1].eventNumber;
                me.onBeforeSnapshotSave(item, me.opts.eventsHandlers, function(hookErr, finalData){
                  // create a new record;
                  var newRecord = {
                    key: itemId + ':latest',
                    // the latest version number
                    version: lastEventNumber,
                    // a flag to check if there are new event to pull
                    // @TODO: set dirty to 0. temporarily always dirty to force events pull
                    dirty: 1, 
                    // the item data itself
                    data: JSON.stringify(finalData)
                  };
                  // and store it, overwriting the latest (no need to wait)
                  return me.opts.aerospike.put(latestItemKey, newRecord, function(error, responseRecord){
                    me.log('Snapshot rebuilt and updated');
                    // return data
                    return callback(null, finalData);
                  });
                });
              });
              
            });

          // if we want the lates version and we found it, just return data
          } else {
            me.log('Snapshot is up to date. Just returning it...');
            return callback(null, JSON.parse(record.data));
          }
        }
      });
    }

    this.getSpecificVersion = function(itemId, version, callback){
      var me = this;
      // not an integer? error
      if(!this.isInt(parseInt(version))) {
        me.log('Invalid VERSION');
        return callback({type:'invalid-version'});
      }
      var versionItemKey = new Key(this.opts.aerospikeNamespace, this.opts.category + 's', itemId + ':' + version);
      this.opts.aerospike.get(versionItemKey, (error, record, meta) => {
        
        // no item... let's build it!
        if (error && error.code === AEROSPIKE_ERR_RECORD_NOT_FOUND) {
          me.log('item doesnt exist at version:' + version);

          // let's check the stream
          var streamName = me.opts.category + '-' + itemId;
          return me.opts.esRange.getStreamRange(streamName, 'start', version, function(error, events){
            // some error?
            if(error){
              return callback({ entity: m.opts.category, type:'read-error', uuid: itemId, notify_user: true });
            }
            var lastEventNumber = events[events.length - 1].eventNumber;
            // the last eventNumber of the return stream cannot be smaller than the version,
            // if it happens it means that the there is not such event (and so, that version)
            if(lastEventNumber < version){
              me.log('Stream found, but last event is smaller (' + lastEventNumber + ') than the desired: ' + version);
              return callback({ entity: me.opts.category, type:'not-found', uuid: itemId, notify_user: true });
            }
            me.log('Got ' + events.length + ' events. Starting aggregate with snapshot...');
            // calculate the deltas
            return me.rebuildItemFromEvents(events, null, function(rebuildErr, item){
              me.onBeforeSnapshotSave(item, me.opts.eventsHandlers, function(hookErr, finalData){
                // create a new record;
                var newRecord = {
                  key: itemId + ':' + version,
                  // the latest version number
                  version: lastEventNumber,
                  // a flag to check if there are new event to pull
                  // @TODO: set dirty to 0. temporarily always dirty to force events pull
                  dirty: 1, 
                  // the item data itself
                  data: JSON.stringify(finalData)
                };
                // and store it, overwriting the latest (no need to wait)
                return me.opts.aerospike.put(versionItemKey, newRecord, function(error, responseRecord){
                  me.log('Snapshot specific version built and saved');
                  // return data
                  return callback(null, finalData);
                });
              });
            });
            
          });

        // some error
        } else if (error) {
          return callback(error);

        // we found the item
        } else {

          // if we want a specific version and we found it, just return data
          me.log('Yes we have version: ' + version);
          return callback(null, JSON.parse(record.data), true);
          
        }
      });
    }

    this.runEventRecursive = function(events, eventsHandlers, data, callback){
      var me = this;
      if(events[this.eventsIndex]){
        var  me = this;
        var event = events[this.eventsIndex];
        me.log('SNAPSHOT EVENT:' + event.eventType);
        eventsHandlers[event.eventType](event, data, function(err, resultData){
          if(err) return callback(err);
          me.eventsIndex++;
          me.runEventRecursive(events, eventsHandlers, resultData, callback);
        });
      } else {
        callback(null, data);
      }
    }

    this.getIgnoredEvents = function(events){
      var 
        len = events.length, 
        ignored = {  },
        undoEvent = this.opts.undoEvent,
        redoEvent = this.opts.redoEvent;
      for(var i=0; i<len; i++){
        var event = events[i];
        if(event.eventType == undoEvent && event.data.undo_request_uuid){
          ignored[event.data.undo_request_uuid] = true;
        } else if(event.eventType == redoEvent && event.data.redo_request_uuid){
          delete ignored[event.data.redo_request_uuid];
        }
      }
      //console.log('EVENTOS IGNORADOS:', ignored);
      return ignored;
    }

    this.applyUndoRedo = function(events, ignored){
      var 
        len = events.length, 
        out = [],
        undoEvent = this.opts.undoEvent,
        redoEvent = this.opts.redoEvent;
      for(var i=0; i<len; i++){
        var event = events[i];
        if(event.eventType == undoEvent || event.eventType == redoEvent){
          // do not add
        } else if(ignored[event.data.request_uuid]) {
          // do not add
        } else {
          out.push(event);
        }
      }
      return out;
    }
    
    this.rebuildItemFromEvents = function(events, previousSnapshot, onRebuildDoneCallback){
      var me = this;
      me.log('Rebuilding...');
      var 
        data = previousSnapshot || {},
        eventsHandlers = this.opts.eventsHandlers,
        ignoredEvents = this.getIgnoredEvents(events),
        historyAppliedEvents = this.applyUndoRedo(events, ignoredEvents);
      this.eventsIndex = 0;
      this.runEventRecursive(historyAppliedEvents, eventsHandlers, data, function(err, data){
        me.log('...About to return rebuilt data...');
        onRebuildDoneCallback(err, data);
      });
    }

    this.isInt = function(n){
      return Number(n) === n && n % 1 === 0;
    }

  };